#!/bin/bash

# Set crash and burn flag
set -e

# Extract AMI ID for this region
amiId=$( grep -P "$bamboo_region" ami.txt | grep -Po 'ami-\w+' )
echo "amiId=$amiId" >> inject.txt

