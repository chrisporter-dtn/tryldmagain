#!/bin/bash

set -e
work=`pwd`

echo "cleaning and packing repositories"
repoCount=$( env | grep -P '^bamboo_planRepository_\d+_name=\S*$' | wc -l )
for i in $( seq $repoCount ); do
   # Print info for logs
   varPrefix=bamboo_planRepository_${i}
   var=${varPrefix}_name && eval repoName=\$$var
   var=${varPrefix}_branchName && eval branchName=\$$var
   var=${varPrefix}_revision && eval thisCommit=\$$var
   var=${varPrefix}_previousRevision && eval prevCommit=\$$var
   if [ "$thisCommit" != "$prevCommit" ]; then
      repoStatus='UPDATED'
   else
      repoStatus='no change'
   fi
   echo "... $repoName | $branchName | $thisCommit | $repoStatus"

   # Clean and package
   cd $work/$repoName && find . | grep /.git$ | xargs rm -rf
   cd $work && tar -czf $repoName.tgz $repoName/
done

# Check / set environment
#branchName=$bamboo_branchName
if ! [ -z $bamboo_ManualBuildTriggerReason_userName ]; then
   runType="manual"
   userName=$bamboo_ManualBuildTriggerReason_userName
else
   runType="automated"
   userName="see commit logs"
fi
version=$( date -u +"%Y%m%d%H%M%S" )

buildEnv=${bamboo_environment,,} # force lowercase
if [ "$buildEnv" == "production" ]; then
   longEnv="Production"
   shortEnv="prod"
elif [ "$buildEnv" == "staging" ]; then
   longEnv="Staging"
   shortEnv="stg"
elif [ "$buildEnv" == "development" ]; then
   longEnv="Development"
   shortEnv="dev"
else
   echo "Unrecognized build environment: $buildEnv. Must be production, staging, or development"
   exit 1
fi

echo "run configuration:"
echo "... type: $runType"
#echo "... branch: $branchName"
echo "... user: $userName"
echo "... version: $version"
echo "... environment: $longEnv"

# # Base AMI
baseAMI=$( grep -P "$bamboo_Region" baseami.txt | grep -Po 'ami-\w+' ) #basewdssiiami, pulled in as artifact from basewdssiiami build plan

# Build AMI
echo "building AMI"
cd $work # Template uses relative paths
packer build \
   -var "aws_access_key=$bamboo_custom_aws_accessKeyId" \
   -var "aws_secret_key=$bamboo_custom_aws_secretAccessKey_password" \
   -var "aws_session_token=$bamboo_custom_aws_sessionToken_password" \
   -var "version=$version" \
   -var "shortEnv=$shortEnv" \
   -var "longEnv=$longEnv" \
   -var "userName=$userName" \
   -var "baseAMI=$baseAMI" \
   $work/wdssiildm/etc/build/packer-template.json | tee packer.log
echo "... build complete"
es=${PIPESTATUS[0]}

grep -Po '\w+-\w+-\d+: ami-\w+$' packer.log > ami.txt

exit $es
