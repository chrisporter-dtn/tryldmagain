#!/bin/bash

# ----------------------------
# USER CHANGE (ldm->root)
# ----------------------------
# http://unix.stackexchange.com/questions/70859/why-doesnt-sudo-su-in-a-shell-script-run-the-rest-of-the-script-as-root
[ $( whoami ) = root ] || exec sudo -E -u root $0

# As Suggested at https://www.packer.io/intro/getting-started/provision.html to allow OS to fully boot.
sleep 30

# Useful vars
LOCAL_SRC_DIR=/usr/local/src

# Install updates
yum -y update

# Install MSS code
tar -xz -C $LOCAL_SRC_DIR -f /tmp/install_scripts.tgz && rm -f /tmp/install_scripts.tgz
cd $LOCAL_SRC_DIR/install_scripts
./install_mss-client.bash
rm -rf $LOCAL_SRC_DIR/install_scripts

# Create and change permissions of /mssdata directory
mkdir /mssdata
chown ldm:ldm /mssdata

# Install project
# # Untar code
tar -xz -C $LOCAL_SRC_DIR -f /tmp/wdssiildm.tgz && rm -f /tmp/wdssiildm.tgz

# # Install misc. files
cd $LOCAL_SRC_DIR/wdssiildm/etc/
chmod --reference=/usr/local/ldm ldm && chown -R ldm:ldm ldm/ && rsync -a ldm/ ~ldm/

# # Install project code
pip install $LOCAL_SRC_DIR/wdssiildm/ --extra-index-url https://artifactory.prod.us-east-1.wdtinc.com/artifactory/api/pypi/pypi-local/simple

# # Cleanup
rm -rf $LOCAL_SRC_DIR/wdssiildm/

