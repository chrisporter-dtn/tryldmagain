#!/usr/bin/env/python
"""
WDSSII LDM update / monitor script

Update ldmd.conf and restart WDSSII LDM as needed

Copyright (c) 2016 Weather Decision Technologies, Inc. All rights reserved.
"""

# NOTE: It is essential that all Popen calls (e.g., subprocess.call) that aren't guaranteed to exit immediately be called with close_fds=True to make sure the socket is released in the child process. This is especially key for ldmadmin start / restart as the child process persists.

# =============================================================================

# Built-in modules
import os
import sys
import argparse
import socket
import signal
import re
import time
import subprocess
import filecmp
import logging

# Third-party modules
import boto3
from wdt_logging.aws import SNSHandler

# Project specific modules
from .util import acquire_lock, release_lock, Timeout, _timeout

# =============================================================================

# Prep logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
log_sh = logging.StreamHandler(sys.stdout)
log_sh.setLevel(logging.INFO)
logger.addHandler(log_sh)

# Various incarnations of ldmd.conf
ORIG_FN = '/usr/local/ldm/etc/ldmd.conf'
UPDATE_FN = '{0}.UPDATING'.format(ORIG_FN)
PREV_FN = '{0}.PREVIOUS'.format(ORIG_FN)
ERR_FN_TEMPLATE = '{0}.ERROR.{{0}}'.format(ORIG_FN)

# LDM process id file
PID_FN = '/usr/local/ldm/ldmd.pid'

# Configure timeout on SIGALRM
signal.signal(signal.SIGALRM, _timeout)

# =============================================================================


def scan_ddb(ddb_table):
    """Scan DDB table to retrieve LDM request entries"""
    client = boto3.resource('dynamodb').meta.client
    response = client.scan(TableName=ddb_table, ProjectionExpression='pid,host,pattern')
    items = response.get('Items', [])  # TODO: Update to handle not grabbing all items on first request
    return items


def ping_host(host):
    """Make sure LDM can resolve the host to prevent build up of zombie requests"""
    rc = subprocess.call(['ldmping', '-i', '0', host], close_fds=True)
    return rc == 0


def load_requests(ddb_table):
    """Load requests from DDB, sanitize the entries, validate the hosts, and construct the request strings"""
    # Get DDB table requests
    items = scan_ddb(ddb_table)

    # Sanitize entries and map host to pattern
    # TODO: What if host / pattern is a list, map, etc.?
    host2pattern = {}
    for item in items:
        # This will take some testing
        project, host, pattern = item.get('project', ''), item.get('host', ''), item.get('pattern', '')
        try:
            safe_host = re.sub(r'\s', '', host.encode('unicode_escape'))
            safe_pattern = re.sub(r'\s', '', pattern.encode('unicode_escape'))
        except:
            safe_host, safe_pattern = '', ''

        if safe_host and safe_pattern:
            host2pattern.setdefault(safe_host, set([])).add(safe_pattern)
        else:
            # TODO: Test
            info_str = '"{0}", "{1}", "{2}"'.format(project, host, pattern)
            logger.warning('host or pattern string empty after sanitizing: {0}'.format(info_str))

    # Build request strings (use sort to ensure consistent ordering for file comparison)
    request_strs = []
    for host in sorted(host2pattern.keys()):
        reachable = ping_host(host)
        if reachable:
            for pattern in sorted(host2pattern[host]):
                request_str = 'REQUEST\tANY\t{0}\t{1}\n'.format(pattern, host)
                request_strs.append(request_str)
        else:
            logger.warning('host {0} is not reachable - ignoring'.format(host))

    return request_strs


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ddb_table', default=None, help='ddb requests table name')
    parser.add_argument('--log_sns', default=None, help='sns publish-on-error topic ARN')
    args = parser.parse_args()

    # Add SNS handler if requested
    if args.log_sns is not None:
        log_snsh = SNSHandler(args.log_sns, subject_prefix='wdssiildm-update')
        log_snsh.setLevel(logging.ERROR)
        logger.addHandler(log_snsh)

    # Sanity check
    if args.ddb_table is None:
        logger.error('missing required argument: ddb_table')
        sys.exit()

    # Acquire lock to prevent duplicate processes
    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    if not acquire_lock(s, 'update'):
        logger.warning('process is already running - exiting')
        sys.exit()

    try:
        # Update ldmd.conf files
        signal.alarm(2*60)
        updated = False
        try:
            updated = update_ldmd(args.ddb_table)
        except Timeout as e:
            logger.error('ldmd_update timed out - continuing with run_ldm')
        except Exception as e:
            logger.exception('unhandled exception occurred during updated_ldmd')
        finally:
            signal.alarm(0)

        # Make sure LDM is running and restart it if an update has occurred
        signal.alarm(5*60)  # NOTE: this may be updated during run_ldm
        try:
            run_ldm(updated)
        except Timeout as e:
            logger.error('run_ldm timed out - LDM MAY NOT BE RUNNING')
        except Exception as e:
            logger.exception('unhandled exception occurred during run_ldm - LDM MAY NOT BE RUNNING')
        finally:
            signal.alarm(0)

    except Exception as e:
        logger.exception('unhandled exception occurred during main - LDM MAY NOT BE RUNNING')
    finally:
        release_lock(s)
        s = None


def update_ldmd(ddb_table):
    """
    Create an updated ldmd.conf file based on requests DDB table contents
    """

    # Get request strings and stop if no requests made it through. In this case the problem is
    # ... probably local so we don't want to overwrite the working file with an empty one
    request_strs = load_requests(ddb_table)
    if not request_strs:
        logger.warning('no requests passed sanitization / host checks - no update performed')
        return False

    # Determine which account this is running in
    ip = socket.gethostbyname(socket.gethostname())

    # Build new ldmd.conf file
    with open(UPDATE_FN, 'w') as fp2:
        fp2.write('EXEC\t"pqact"\n')
        fp2.write('ALLOW\tANY\t^((localhost|loopback)|(127\\.0\\.0\\.1\\.?$))\n')
        fp2.write('ALLOW\tANY\t10.110.\n')
        fp2.write('ALLOW\tANY\t172.16.100.206\n')
        if (ip[:6] == '10.105'):
            fp2.write('ALLOW\tANY\t10.105.\n')
            fp2.write('ALLOW\tANY\t172.16.100.28\n')
            fp2.write('ALLOW\tANY\t172.16.100.136\n')
        elif (ip[:6] == '10.100'):
            fp2.write('ALLOW\tANY\t10.105.\n')
            fp2.write('ALLOW\tANY\t10.100.\n')        
            fp2.write('ALLOW\tANY\t172.16.100.28\n')
            fp2.write('ALLOW\tANY\t172.16.100.136\n')
        for request_str in request_strs:
            fp2.write(request_str)

    # Update files if the old and new differ
    same = filecmp.cmp(ORIG_FN, UPDATE_FN)
    if not same:
        logger.info('overwriting ldmd.conf with updated file')
        os.rename(ORIG_FN, PREV_FN)
        os.rename(UPDATE_FN, ORIG_FN)
    else:
        logger.info('updated ldmd.conf is identical to original - no update performed')

    return not same


def run_ldm(updated):
    """
    Run LDM if it is not running, or, if updated=True, restart LDM regardless. Progressively more
    extreme measures will be taken to start LDM if not initially successful. If an error occurs and
    updated=True, we attempt to rollback to the previous ldmd.conf file. Further automated updates
    are disabled (by keeping this process alive) if successful. This is to keep some data flowing
    until whatever got through the checks can be tracked down. If the rollback fails, updates continue
    as usual in the hope that the problem will fix itself (after notifying somebody to look into it).
    """

    # Try to recreate ldmd.pid file if it doesn't exist. Alert, but don't die. If we're not
    # ... successful and there's no update things will at least keep running
    if not os.path.exists(PID_FN):
        logger.error('... pid file does not exist - attempting to recreate. NOTE: no success / failure messages will follow (unless a restart is required) to prevent downtime. A manual check should be performed')
        cmd = "pgrep -P 1 -x ldmd -u ldm -o > {0}".format(PID_FN)
        subprocess.call([cmd], shell=True, close_fds=True)

    # Perform start / restart if necessary
    ldm_running = ldmadmin_isrunning()
    if ldm_running and updated:
        logger.info('attempting to restart LDM')
        ldm_running = ldmadmin_restart()
    elif not ldm_running:
        logger.info('attempting to start LDM')
        ldm_running = ldmadmin_start()
    clean_start = ldm_running  # Indicates start / restart went through cleanly

    # Handle the bad stuff
    if not ldm_running:
        logger.error('... error occurred during ldm start / restart. Additional notifications to follow (this may take several minutes).')
        if updated:
            err_fn = ERR_FN_TEMPLATE.format(time.strftime('%Y%m%d%H%M%S', time.gmtime()))
            logger.warning('... reverting to previous ldmd.conf. Erroring ldmd.conf is located at {0}'.format(err_fn))
            os.rename(ORIG_FN, err_fn)
            os.rename(PREV_FN, ORIG_FN)

    if not ldm_running:
        logger.warning('... ldm is not running - attempting to stop, clean, and start ldm')
        ldm_running = ldmadmin_stop_clean_start()

    if not ldm_running:
        logger.warning('... ldm is not running - attempting to tear down existing processes and restart ldm (this may take several minutes)')
        signal.alarm(7*60)  # We've got a big queue - this takes some time
        ldm_running = ldmadmin_tear_down_and_start()

    if not ldm_running:
        # Would typically handle cases where the ldmd.pid file disappears
        logger.warning('... ldm is not running - attempting to kill all *ldmd* processes and restart as a last resort (this may take several minutes)')
        signal.alarm(7*60)  # We've got a big queue - this takes some time
        ldm_running = ldmadmin_kill_and_start()

    if not ldm_running:
        logger.error('failed to restart ldm - LDM IS NOT RUNNINIG')
    elif not clean_start:
        if updated:
            logger.error('error occurred during ldm restart but rolled back successfully. PREVENTING AUTOMATED UPDATES TO ldmd.conf UNTIL MANUALLY KILLED (process {0}). NOTE: ldmadmin stop must be run after killing the process to properly release the socket lock'.format(os.getpid()))
            signal.alarm(0)
            while True:
                time.sleep(5)
        else:
            logger.error('ldm failed to start cleanly, but was eventually started')
    else:
        logger.info('ldm is running')


def ldmadmin_isrunning():
    """Determine LDM status. Returns True if running, False otherwise"""
    rc = subprocess.call(['ldmadmin', 'isrunning'], close_fds=True)
    return rc == 0


def ldmadmin_start():
    """Start LDM"""
    rc = subprocess.call(['ldmadmin', 'start'], close_fds=True)
    return rc == 0


def ldmadmin_restart():
    """Restart LDM"""
    rc = subprocess.call(['ldmadmin', 'restart'], close_fds=True)
    return rc == 0


def ldmadmin_stop_clean_start():
    """Stop, clean, and start LDM"""
    subprocess.call(['ldmadmin', 'stop'], close_fds=True)
    subprocess.call(['ldmadmin', 'clean'], close_fds=True)
    rc = subprocess.call(['ldmadmin', 'start'], close_fds=True)
    return rc == 0


def ldmadmin_tear_down_and_start():
    """Stop, delete and recreate the queue, clean, and start LDM"""
    subprocess.call(['ldmadmin', 'stop'], close_fds=True)
    subprocess.call(['ldmadmin', 'delqueue'], close_fds=True)
    subprocess.call(['ldmadmin', 'mkqueue'], close_fds=True)
    subprocess.call(['ldmadmin', 'clean'], close_fds=True)
    rc = subprocess.call(['ldmadmin', 'start'], close_fds=True)
    return rc == 0


def ldmadmin_kill_and_start():
    """Kill all ldmd processes and call ldmadmin_tear_down_and_start()"""
    subprocess.call(["kill $( pgrep -x ldmd -u ldm )"], shell=True, close_fds=True)
    return ldmadmin_tear_down_and_start()


if __name__ == '__main__':
    main()
