#!/usr/bin/env/python
"""
Utilities for WDSSII LDM code

Copyright (c) 2016 Weather Decision Technologies, Inc. All rights reserved.
"""

# NOTE: It is essential that all Popen calls (e.g., subprocess.call) that aren't guaranteed to exit immediately be called with close_fds=True to make sure the socket is released in the child process. This is especially key for ldmadmin start / restart as the child process persists.

# =============================================================================

# Built-in modules
import signal

# Third-party modules
pass

# Project specific modules
pass

# =============================================================================


def acquire_lock(s, process):
    """Acquire lock and prevent new processes from running"""
    try:
        s.bind('\0wdssiildm{0}lock'.format(process))
    except socket.error as e:
        return False
    return True


def release_lock(s):
    """
    Release socket and allow new processes to run
    NOTE: You should set the socket variable to None after calling this function
    """
    s.close()


class Timeout(Exception):
    """Exception class denoting timeout"""
    pass


def _timeout(*args):
    """Raise Timeout exception on SIGALRM"""
    raise Timeout()
