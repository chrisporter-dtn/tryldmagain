#!/usr/bin/env/python
"""
WDSSII LDM monitor script

Monitor LDM statistics (currently age and nprods only)

Copyright (c) 2016 Weather Decision Technologies, Inc. All rights reserved.
"""

# NOTE: It is essential that all Popen calls (e.g., subprocess.call) that aren't guaranteed to exit immediately be called with close_fds=True to make sure the socket is released in the child process. This is especially key for ldmadmin start / restart as the child process persists.

# =============================================================================

# Built-in modules
import sys
import argparse
import socket
import signal
from datetime import datetime, timedelta
import logging

# Third-party modules
from wdt_logging.aws import SNSHandler

# Project specific modules
from .util import acquire_lock, release_lock, Timeout, _timeout

# =============================================================================

# Prep logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
log_sh = logging.StreamHandler(sys.stdout)
log_sh.setLevel(logging.INFO)
logger.addHandler(log_sh)

# Two latest metrics file locations
CMETRICS_FN = '/usr/local/ldm/var/logs/metrics.txt'
PMETRICS_FN = '/usr/local/ldm/var/logs/metrics.txt.1'

# Misc. static entities
METRICS_PER_LINE = 18

# Alert thresholds
MIN_QUEUE_AGE = 600
MAX_NPRODS = 350000

# Configure timeout on SIGALRM
signal.signal(signal.SIGALRM, _timeout)

# =============================================================================


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--log_sns', default=None, help='sns publish-on-error topic ARN')
    args = parser.parse_args()

    # Add SNS handler if requested
    if args.log_sns is not None:
        log_snsh = SNSHandler(args.log_sns, subject_prefix='wdssiildm-monitor')
        log_snsh.setLevel(logging.ERROR)
        logger.addHandler(log_snsh)

    # Acquire lock to prevent duplicate processes
    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    if not acquire_lock(s, 'monitor'):
        logger.warning('process is already running - exiting')
        sys.exit()

    signal.alarm(30)
    try:
        assess_metrics()
    except Timeout as e:
        logger.error('assess_metrics timed out')
    except Exception as e:
        logger.exception('unhandled exception occurred during assess_metrics')
    finally:
        signal.alarm(0)
        release_lock(s)
        s = None


def load_metrics(fns):
    """
    Load and parse metrics files
    """
    # Load and sort lines by timestamp
    lines = []
    for fn in fns:
        try:
            with open(fn, 'r') as fp:
                file_lines = fp.readlines()
            lines.extend(file_lines)
        except:
            pass
    lines = sorted(lines)

    # Parse lines into separate values
    metrics = {}
    metrics_dt, metrics_age, metrics_nprods = [], [], []
    for line in lines:
        try:
            line_metrics = line.split()
            assert len(line_metrics) == METRICS_PER_LINE
            line_dt = datetime.strptime(line_metrics[0], '%Y%m%d.%H%M%S')
            line_age = int(line_metrics[6])
            line_nprods = int(line_metrics[7])
        except:
            logger.warning('ignoring invalid line {0}'.format(line))
            continue

        metrics_dt.append(line_dt)
        metrics_age.append(line_age)
        metrics_nprods.append(line_nprods)

    # Construct metrics dict
    metrics['count'] = len(metrics_dt)
    metrics['dt'] = metrics_dt
    metrics['age'] = metrics_age
    metrics['nprods'] = metrics_nprods

    return metrics


def assess_metrics():
    """
    Create warning / error messages based on metric values
    """
    metrics = load_metrics([CMETRICS_FN, PMETRICS_FN])

    # Rough sanity checks
    if metrics['count'] <= 0:
        logger.error('no metrics available - stopping')
        sys.exit()

    oldest = metrics['dt'][0]
    if (datetime.utcnow() - oldest) <= timedelta(minutes=30):
        logger.warning('no old metrics, assuming new instance (oldest metric valid {0}) - stopping'.format(oldest.isoformat()))
        sys.exit()

    latest = metrics['dt'][-1]
    if (datetime.utcnow() - latest) > timedelta(minutes=30):
        logger.error('no recent metrics (latest metric valid {0}) - stopping'.format(latest.isoformat()))
        sys.exit()

    # Check queue age
    latest_age = metrics['age'][-1]
    if latest_age < MIN_QUEUE_AGE:
        logger.error('queue age is low ({0} < {1} seconds)'.format(latest_age, MIN_QUEUE_AGE))

    # Check number of slots
    latest_nprods = metrics['nprods'][-1]
    if latest_nprods > MAX_NPRODS:
        logger.error('nprods is high ({0} > {1})'.format(latest_nprods, MAX_NPRODS))


if __name__ == '__main__':
    main()
