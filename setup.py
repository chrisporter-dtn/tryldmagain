from setuptools import setup

setup(
    name='wdssiildm',
    version='1.0.1',
    description='Monitor and update WDSSII LDM LDM',
    long_description='Monitor and update WDSSII LDM LDM',
    packages=['wdssiildm'],
    install_requires=['boto3', 'wdt_logging[aws]'],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'wdssiildmupdate=wdssiildm:update',
            'wdssiildmmonitor=wdssiildm:monitor',
        ],
    },
    author='Weather Decision Technologies',
    author_email='amackenzie@wdtinc.com',
    classifiers=(
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6'
        'Programming Language :: Python :: 2.7'
    ),
)
